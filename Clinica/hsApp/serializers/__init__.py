from .usuarioSerializer import usuarioSerializer
from .medicoSerializer import medicoSerializer
from .pacienteSerializer import pacienteSerializer
from .DiagSerializer import DiagSerializer
