from hsApp.models.DiagPacientes import DiagPacientes
from rest_framework import serializers

class DiagSerializer(serializers.ModelSerializer):
    class Meta:
        model = DiagPacientes
        fields = ['id', 'fecha', 'diagnostico', 'tratamiento', 'evolucion', 'medTratante', 'estado']