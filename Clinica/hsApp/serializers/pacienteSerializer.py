from hsApp.models.Pacientes import Pacientes
from rest_framework import serializers

class pacienteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pacientes
        fields = ['id', 'nombre', 'apellido', 'antecedentes']
        