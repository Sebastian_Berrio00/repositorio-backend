from rest_framework import serializers
from hsApp.models.Usuarios import Usuarios
from hsApp.models.Pacientes import Pacientes
from hsApp.models.Medicos import Medicos
from hsApp.models.DiagPacientes import DiagPacientes


class usuarioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Usuarios
        fields = ['id', 'password', 'rol', 'TipoDoc', 'nombre', 'apellidos','email', 'estado', 'direccion', 'telefono']



    def to_representation(self, obj):
        User = Usuarios.Objects()
        return {
         'id': User.id,
         'password': User.password,
         'rol': User.rol,
         'Tipo de documento': User.TipoDoc,
         'nombre': User.nombre,
         'Apellidos': User.apellidos,
         'email': User.email,
         'estado': User.estado,
         'direccion':User.direccion,
         'telefono':User.telefono,

}