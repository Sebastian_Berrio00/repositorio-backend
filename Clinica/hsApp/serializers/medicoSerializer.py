from hsApp.models.Medicos import Medicos
from rest_framework import serializers

class medicoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Medicos
        fields = ['id', 'nombre', 'apellido', 'especialidad', 'turno']
        