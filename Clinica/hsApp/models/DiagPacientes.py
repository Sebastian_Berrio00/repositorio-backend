from django.db import models

from hsApp.models.Pacientes import Pacientes
from .Usuarios import Usuarios

class DiagPacientes(models.Model):
    
    Id = models.ForeignKey(Pacientes, related_name='Pacientes', on_delete=models.CASCADE)
    FechaDiag = models.DateField('Fecha de Diagnóstico')
    Diagnostico=models.CharField('Diagnóstico', max_length=100)
    Tratamiento=models.CharField('Tratamiento', max_length=250)
    Evolucion=models.CharField('Evolución', max_length=250)
    MedicoTratante=models.CharField('Médico Tratante', max_length=200)
    isActive = models.BooleanField(default=True)