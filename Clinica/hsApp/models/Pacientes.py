from django.db import models
from .Usuarios import Usuarios

class Pacientes(models.Model):
    
    Id = models.ForeignKey(Usuarios, related_name='Pacientes', on_delete=models.CASCADE)
    IdPaciente= models.IntegerField(primary_key=True)
    Antecedentes = models.CharField('Antecedentes', max_length = 45)
    isActive = models.BooleanField(default=True)