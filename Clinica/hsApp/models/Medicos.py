from django.db import models
from .Usuarios import Usuarios

class Medicos(models.Model):
    
    Id = models.ForeignKey(Usuarios, related_name='Medicos', on_delete=models.CASCADE)
    Especialidad = models.CharField('Especialidad', max_length = 45)
    turnoMed = models.CharField('Turnos de los Medicos', max_length = 45)
    isActive = models.BooleanField(default=True)