from .Pacientes import Pacientes
from .Usuarios import Usuarios
from .Medicos import Medicos
from .DiagPacientes import DiagPacientes