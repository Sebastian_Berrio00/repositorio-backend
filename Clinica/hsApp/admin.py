from django.contrib import admin
from .models.Usuarios import Usuarios
from .models.Pacientes import Pacientes
from .models.Medicos import Medicos
from .models.DiagPacientes import DiagPacientes

admin.site.register(Usuarios)
admin.site.register(Pacientes)
admin.site.register(Medicos)
admin.site.register(DiagPacientes)

# Register your models here.
